## demo app - developing with Docker

Diese Demo App zeigt ein Beispiel wie eine Node.JS "Backend Applikation" mit einem MongoDB Server verbunden werden kann.

Damit diese App gestartet werden kann, braucht es folgende Befehle die in einem Container ausgeführt werden müssen:

*[ ] kopieren des ./app Ordners in einen Ordner im Container (z.B.: /home/app)
*[ ] kopieren der package.json und package-lock.json Dateien in den Container (auch in /home/app)
*[ ] Wechsel des Arbeitsverzeichnisses zu /home/app im Container
*[ ] Ausführen von npm install, damit die notwendigen Node Pakete installiert werden
*[ ] Ausführen von npm server.js als Einstiegspunkt in den Container.

## Inhalt des originalen ReadMe Files

#### To start the application

Step 1: start mongodb and mongo-express

    docker-compose -f docker-compose.yaml up
    
_You can access the mongo-express under localhost:8080 from your browser_
    
Step 2: in mongo-express UI - create a new database "my-db"

Step 3: in mongo-express UI - create a new collection "users" in the database "my-db"       
    
Step 4: start node server 

    node server.js
    
_You can access the application under localhost:3000 from your browser_

#### To build a docker image from the application

    docker build -t my-app:1.0 .       
    
The dot "." at the end of the command denotes location of the Dockerfile.
