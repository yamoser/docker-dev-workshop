# Docker Workshop für reproduzierbare Entwicklungsumgebungen

In diesem Workshop wollen wir anschauen, wie uns Container (in diesem Fall auf Basis von Docker) unterstützen können, um reproduzierbare Entwicklungsumgebungen benutzen können.

## Aufbau

Dieses Repository ist in verschiedene "Projekte" welche unterschiedlich komplexe Entwicklungsumgebungen beinhaltet.

 - Projekt_1 - Beinhaltet eine einfache Node App die regelmässig ein Output auf die Kommandozeile ausgibt.
 - Projekt_2 - Beinhaltet eine einfache Node App mit Frontend und DB. Das Frontend ist über eine URL zugreifbar und zeigt eine einfache Webseite an.
 - Projekt_3 - Beinhaltet eine Node App mit Front- und Backend inklusive eines MySQL Datenbankservers. Das Frontend ist über eine URL zugreifbar und gibt einen einfachen Satz aus inklusive der MySQL Serverversion.
 - Projekt_4 - Python Flask Projekt inklusive Datenbankserver. Die Frontend Applikation ist über eine URL zugreifbar und gibt Zugang zu einer Web App.

In allen Projektordnern hat es ein spezifisches README.md File, dass die Funktion sowie den Aufbau des Projekts beschreibt, inklusive Aufgaben zum Docker Teil.

## Wie benutze ich dieses Repo?

Das Repository ist so aufgebaut, dass es geforkt und dann an den Docker Aufgaben, in den jeweiligen Projekten, gearbeitet werden kann. In diesem Workshop geht es nicht unbedingt um den Software Code sondern wie man Dockerfile und docker-compose.yml Files erstellen / ergänzen kann um die Entwicklungsumgebung aufbauen zu können.

## Voraussetzungen

Virtuelle Maschine mit Docker und git, sowie einem präferierten Editor oder einer Entwicklungsumgebung. Empfehlung: Visual Studio Code.

## Copyright und verwendeter Code.
Dieser Workshop Code ist mit der MIT License lizenziert und darf frei verwendet werden. Er beinhaltet Code von folgenden Projekten.

- Projekt 1 - Techworld with Nana (Nana Janashia) - https://gitlab.com/nanuchi/techworld-js-docker-demo-app - All rights reserved
- Projekt 2 - Techworld with Nana (Nana Janashia) - https://gitlab.com/nanuchi/techworld-js-docker-demo-app/-/tree/master/ - All rights reserved
- Projekt 3 - Awesome Docker Compose Repository - https://github.com/docker/awesome-compose - Creative Commons License
- Projekt 4 - Testdriven App - https://github.com/Somaimo/testdriven-app - MIT License